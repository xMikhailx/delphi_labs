unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Memo1: TMemo;
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    BitBtn1: TBitBtn;
    ListBox1: TListBox;
    RadioGroup1: TRadioGroup;
    Label2: TLabel;
    Edit2: TEdit;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    ScrollBar1: TScrollBar;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ScrollBar1Scroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  x,y,X_kon,X_nach,D: real;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  x:= StrToFloat(Edit1.Text);
  y:= x/400;
  Memo1.Lines.Add(FormatFloat('####.## ������� =', x) +
  FormatFloat('####.## ������', y));
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
  if (Edit1.Text='') or (ListBox1.ItemIndex <0) then Exit;
  Case RadioGroup1.ItemIndex of
    1: begin
      if (Edit2.Text='') then Exit;
      X_nach:=StrToFloat(Edit1.Text);
      X_kon:=StrToFloat(Edit2.Text);
      D:= (X_kon -X_nach)/10;
    end;
    0: begin
      X_nach:=StrToFloat(Edit1.Text);
      X_kon:=X_nach;
      D:=1;
    end;
  end;
  x:= X_nach;
  while x <= X_kon do
  begin
    case ListBox1.ItemIndex of
      0: y:= x/400;
      1: y:= x/16380;
      2: y:= x/28.35;
      3: y:= x/28.35*16;
      4: y:= 437.5*x/28.35;
    end;
    Memo1.Lines.Add(FormatFloat('#####.##### �����=',x)+ FormatFloat('#####.#####',y) + ListBox1.Items[ListBox1.ItemIndex]);
    x:= x + D;
  end;
  Edit1.Clear;
  Edit1.SetFocus;
  Edit2.Clear;
end;
  {x:= StrToFloat(Edit1.Text);
  case ListBox1.ItemIndex of
    0: y:= x/400;
    1: y:= x/16380;
    2: y:= x/28.35;
    3: y:= x/28.35*16;
    4: y:= 437.5*x/28.35;
  end;
  Memo1.Lines.Add(FormatFloat('#####.##### �����=',x)+ FormatFloat('#####.##### ', y) + ListBox1.Items[ListBox1.ItemIndex]);
  Edit1.Clear;
  Edit1.SetFocus;
end;}

procedure TForm1.RadioGroup1Click(Sender: TObject);
begin
  Case RadioGroup1.ItemIndex of
    0: begin
      Label1.Caption := '������� �������� ���� � �������';
      Edit2.Hide;
      Label2.Hide;
    end;
    1: begin
      Label1.Caption := '��������� �������� ����';
      Edit2.Show;
      Label2.Show;
    end;
  end;
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
  If CheckBox1.State = cbChecked then
    Memo1.Font.Style:=[fsItalic]
  Else Memo1.Font.Style:=[];
end;

procedure TForm1.ScrollBar1Scroll(Sender: TObject; ScrollCode: TScrollCode;
  var ScrollPos: Integer);
begin
 x:= ScrollBar1.Position;
 Edit1.Text:= FloatToStr(x);
 RadioGroup1.ItemIndex :=0;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  Edit1.SetFocus;
end;

end.
