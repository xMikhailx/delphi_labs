unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Grids;

type
  TLab3 = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StringGrid1: TStringGrid;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure StringGrid1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure RadioGroup2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const nn=20;  mm=5;
//������������ ���������� ��������� =20.
// ������������ ���������� ��������� =5.
pp: array [0..1] of string[1] = ('m', 'g'); //������, ���������� ������� �������.

var
  Lab3: TLab3;
  n: integer;
  f: array [1..nn] of string [30];
  p: array [1..nn] of string [1];
  a: array [1..nn, 1..mm] of real;
  ss: array [1..nn] of real;
//n - ������� ����� ��������. ���������� ���������;
//f - ������ �������;
//p - ������ ��� ������������� ����;
//a - ������ ������;
//ss - ������ ��� �������� ����� ���������;


implementation

{$R *.dfm}

procedure TLab3.BitBtn1Click(Sender: TObject);
begin
// ���������� ������ "��������". ������������ ���� ������� � ����.
// ��������� �������� ������� � ������������� ������� �������.
     n:= n+1;
     f[n]:= Edit1.Text;
     p[n]:= pp[RadioGroup1.ItemIndex];
     StringGrid1.Cells[0,n]:=f[n];
     Edit1.SetFocus;
     Edit1.Clear;
end;

procedure TLab3.FormActivate(Sender: TObject);
begin
// ���������� ��� ������� �������� ������������� ������
// �������. ��������� ���������� �������� ������ ��������.
  n:=0;
  Edit1.SetFocus;
  With StringGrid1 do
  begin
    Cells[1,0]:= '������';
    Cells[2,0]:= '�������.';
    Cells[3,0]:= '�������';
    Cells[4,0]:= '������.��.';
    Cells[5,0]:= '������� ����';
  end;
end;

procedure TLab3.StringGrid1SetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
//������� ��������� ��� ���������� ��������������
// ������ (ACol, ARow). � ��������� Value ����������
// �������� ��������� ������ ��� �������������� ������.
var
b: real; j:byte;
//b - ������������ �������� ������ ��������������.
//j - �������� ����� ��� �������� ������ ������� ������� a.
begin 
 With StringGrid1 do
  Begin
   If   Value = ' ' then Exit; //���� ������ �� �������, �� ����� �� ���������
   //������ ����������� �����.
    Try b:=StrToFloat(Value)
    Except ShowMessage('�� ������ ������! ');
    b:=0;
    end; //����� ����������� �����.
    //������������ ������� ��������� ������.
    If (b<1) or (b>5) then begin
    Cells[ACol,ARow]:= ' ';
    Exit;
    end;
    Cells[ACol,ARow]:= Value;//�������� ������ � �������.
    a[ARow,ACol]:=b; //�������� ������ � ������.
    //����� ������� �������� �����.
    ss[ARow]:=0;
    for j:=1 to 4 do ss[ARow]:=ss[ARow] + a[ARow,j];
    ss[ARow]:= ss[ARow]/4;
    //���������� ������� ���� � 5-�� ������� �������.
    Cells[5,ARow]:= FormatFloat('##.##', ss[ARow])
    End;
end;

procedure TLab3.RadioGroup2Click(Sender: TObject);
 // ������� ��������� ��� ����� �������� �������
     var S:real; k, i: byte;
      begin
      S:=0; k:=0;
      With RadioGroup2 do
       Begin
       Case ItemIndex of
        0: begin          // ������ �������� ����� ������
            for i:= 1 to n do
            if p[i] = 'm' then begin S:=S + ss[i] ; k:=k + 1 end;
            if k=0 then S:=0 else S:=S/k;  end;
        1: begin          // ������ �������� ����� ������� 
            for i:= 1 to n do
            if p[i] = 'g' then begin S:=S + ss[i] ; k:=k + 1 end;
            if k=0 then S:=0 else S:=S/k;  end;
        2: begin          // ������ ������ �������� ����� 
           for i:= 1 to n do
            S:=S + ss[i] ;
            if n=0 then S:=0 else S:=S/n;      end; end;  end;
            // ����� ���������� � ���� ���������
            Edit2.Text := FormatFloat('##.##',S);
end;

end.
