program Project5;

uses
  Forms,
  Unit4 in 'Unit4.pas' {Lab4},
  Unit2 in 'Unit2.pas' {Lab2},
  Unit3 in 'Unit3.pas' {Lab3},
  Unit5 in 'Unit5.pas' {MainForm},
  Unit1 in 'Unit1.pas' {AboutBox},
  Unit6 in 'Unit6.pas' {PasswordDlg};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TPasswordDlg, PasswordDlg);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TLab4, Lab4);
  Application.CreateForm(TLab2, Lab2);
  Application.CreateForm(TLab3, Lab3);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.
