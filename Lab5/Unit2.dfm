object Lab2: TLab2
  Left = 159
  Top = 321
  BorderStyle = bsNone
  Caption = #203#224#225#238#240#224#242#238#240#237#224#255' '#240#224#225#238#242#224' '#185'2'
  ClientHeight = 616
  ClientWidth = 1289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 461
    Width = 1289
    Height = 155
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    DesignSize = (
      1289
      155)
    object BitBtn1: TBitBtn
      Left = 1016
      Top = -4
      Width = 257
      Height = 145
      Anchors = [akRight, akBottom]
      TabOrder = 0
      Kind = bkClose
    end
    object GroupBox1: TGroupBox
      Left = 24
      Top = 16
      Width = 193
      Height = 137
      Caption = #205#224#241#242#240#238#233#234#224' '#253#234#240#224#237#224
      TabOrder = 1
      object CheckBox1: TCheckBox
        Left = 8
        Top = 24
        Width = 177
        Height = 25
        Caption = #216#240#232#244#242
        TabOrder = 0
        OnClick = CheckBox1Click
      end
    end
    object ScrollBar1: TScrollBar
      Left = 240
      Top = 24
      Width = 217
      Height = 25
      PageSize = 0
      TabOrder = 2
      OnScroll = ScrollBar1Scroll
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 233
    Height = 461
    Align = alLeft
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 175
      Height = 13
      Caption = #194#226#229#228#232#242#229' '#231#237#224#247#229#237#232#229' '#226#229#241#224' '#226' '#227#240#224#236#236#224#245
      WordWrap = True
    end
    object Label2: TLabel
      Left = 8
      Top = 280
      Width = 98
      Height = 13
      Caption = #202#238#237#229#247#237#238#229' '#231#237#224#247#229#237#232#229
      Visible = False
    end
    object Edit1: TEdit
      Left = 8
      Top = 40
      Width = 209
      Height = 21
      TabOrder = 0
    end
    object Button1: TButton
      Left = 8
      Top = 72
      Width = 209
      Height = 25
      Caption = #194#226#238#228
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button1Click
    end
    object ListBox1: TListBox
      Left = 8
      Top = 112
      Width = 209
      Height = 89
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 16
      Items.Strings = (
        #212#243#237#242
        #207#243#228
        #211#237#246#232#255
        #196#240#224#245#236
        #195#240#224#237)
      ParentFont = False
      TabOrder = 2
      OnClick = ListBox1Click
    end
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 208
      Width = 209
      Height = 65
      Caption = #208#224#241#247#229#242
      ItemIndex = 0
      Items.Strings = (
        #197#228#229#237#232#247#237#238#229' '#231#237#224#247#229#237#232#229
        #208#224#241#247#229#242' '#228#235#255' '#232#237#242#229#240#226#224#235#224' '#231#237#224#247#229#237#232#233)
      TabOrder = 3
      OnClick = RadioGroup1Click
    end
    object Edit2: TEdit
      Left = 8
      Top = 312
      Width = 209
      Height = 21
      TabOrder = 4
      Visible = False
    end
  end
  object Memo1: TMemo
    Left = 233
    Top = 0
    Width = 1056
    Height = 461
    Align = alClient
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object MainMenu1: TMainMenu
    Left = 816
    Top = 485
    object N1: TMenuItem
      Caption = #199#224#239#232#241#224#242#252' '#226' '#244#224#233#235
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #209#247#232#242#224#242#252' '#232#231' '#244#224#233#235#224
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #203#224#225#238#240#224#242#238#240#237#251#229' '#240#224#225#238#242#251
      object aaaaaa1: TMenuItem
        Caption = 'Lab3'
        OnClick = aaaaaa1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object aaaaaa2: TMenuItem
        Caption = 'Lab4'
        OnClick = aaaaaa1Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object aaaaaa3: TMenuItem
        Caption = 'Lab5'
        OnClick = aaaaaa1Click
      end
    end
    object aaaaaa4: TMenuItem
      Caption = #194#251#245#238#228
      OnClick = aaaaaa1Click
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 856
    Top = 485
  end
  object OpenDialog1: TOpenDialog
    Left = 896
    Top = 485
  end
end
