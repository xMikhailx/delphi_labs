object Lab3: TLab3
  Left = 532
  Top = 278
  BorderStyle = bsNone
  Caption = #203#224#225#238#240#224#242#238#240#237#224#255' '#240#224#225#238#242#224' '#185'3'
  ClientHeight = 616
  ClientWidth = 1289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 495
    Width = 1289
    Height = 121
    Align = alBottom
    Caption = #206#247#232#241#242#232#242#252
    TabOrder = 0
    object Label1: TLabel
      Left = 216
      Top = 40
      Width = 49
      Height = 13
      Caption = #212#224#236#232#235#232#255
    end
    object Edit1: TEdit
      Left = 24
      Top = 24
      Width = 169
      Height = 21
      TabOrder = 0
      Text = #206#247#232#241#242#232#242#252
    end
    object Edit2: TEdit
      Left = 24
      Top = 64
      Width = 169
      Height = 21
      TabOrder = 1
      Text = #206#247#232#241#242#232#242#252
    end
    object RadioGroup1: TRadioGroup
      Left = 288
      Top = 24
      Width = 153
      Height = 73
      Caption = #207#238#235
      ItemIndex = 0
      Items.Strings = (
        #204#243#230#241#234'.'
        #198#229#237#241#234'.')
      TabOrder = 2
    end
    object RadioGroup2: TRadioGroup
      Left = 456
      Top = 24
      Width = 153
      Height = 73
      Caption = #206#247#232#241#242#232#242#252
      Items.Strings = (
        #209#240#229#228#237#232#233' '#225#224#235#235' '#254#237#238#248#229#233
        #209#240#229#228#237#232#233' '#225#224#235#235' '#228#229#226#243#248#229#234
        #206#225#249#232#233' '#241#240#229#228#237#232#233' '#225#224#235#235)
      TabOrder = 3
      OnClick = RadioGroup2Click
    end
    object BitBtn1: TBitBtn
      Left = 696
      Top = 24
      Width = 121
      Height = 25
      Caption = #199#224#239#232#241#224#242#252
      TabOrder = 4
      OnClick = BitBtn1Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
    object BitBtn2: TBitBtn
      Left = 1120
      Top = 8
      Width = 161
      Height = 105
      TabOrder = 5
      Kind = bkClose
    end
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 1289
    Height = 495
    Align = alClient
    ColCount = 6
    RowCount = 30
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 1
    OnSetEditText = StringGrid1SetEditText
  end
  object MainMenu1: TMainMenu
    Left = 912
    Top = 539
    object N1: TMenuItem
      Caption = #199#224#239#232#241#224#242#252' '#226' '#244#224#233#235
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #203#224#225#238#240#224#242#238#240#237#251#229' '#240#224#225#238#242#251
      object aaaaaa1: TMenuItem
        Caption = 'Lab2'
        OnClick = aaaaaa1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object aaaaaa2: TMenuItem
        Caption = 'Lab4'
        OnClick = aaaaaa1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object aaaaaa3: TMenuItem
        Caption = 'Lab5'
        OnClick = aaaaaa1Click
      end
    end
    object aaaaaa4: TMenuItem
      Caption = #194#251#245#238#228
      OnClick = aaaaaa1Click
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 952
    Top = 539
  end
end
