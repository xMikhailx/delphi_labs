unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Grids, Menus;

type
  TLab3 = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StringGrid1: TStringGrid;
    MainMenu1: TMainMenu;
    SaveDialog1: TSaveDialog;
    N1: TMenuItem;
    N2: TMenuItem;
    aaaaaa1: TMenuItem;
    N3: TMenuItem;
    aaaaaa2: TMenuItem;
    N4: TMenuItem;
    aaaaaa3: TMenuItem;
    aaaaaa4: TMenuItem;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure StringGrid1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure RadioGroup2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aaaaaa1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const nn=20;  mm=5;
//������������ ���������� ��������� =20.
// ������������ ���������� ��������� =5.
pp: array [0..1] of string[1] = ('m', 'g'); //������, ���������� ������� �������.

var
  Lab3: TLab3;
  n: integer;
  f: array [1..nn] of string [30];
  p: array [1..nn] of string [1];
  a: array [1..nn, 1..mm] of real;
  ss: array [1..nn] of real;
//n - ������� ����� ��������. ���������� ���������;
//f - ������ �������;
//p - ������ ��� ������������� ����;
//a - ������ ������;
//ss - ������ ��� �������� ����� ���������;


implementation

uses Unit6, Unit2, Unit4, Unit5;

{$R *.dfm}

procedure TLab3.BitBtn1Click(Sender: TObject);
begin
// ���������� ������ "��������". ������������ ���� ������� � ����.
// ��������� �������� ������� � ������������� ������� �������.
     n:= n+1;
     f[n]:= Edit1.Text;
     p[n]:= pp[RadioGroup1.ItemIndex];
     StringGrid1.Cells[0,n]:=f[n];
     Edit1.SetFocus;
     Edit1.Clear;
end;

procedure TLab3.FormActivate(Sender: TObject);
begin
// ���������� ��� ������� �������� ������������� ������
// �������. ��������� ���������� �������� ������ ��������.
  n:=0;
  Edit1.SetFocus;
  With StringGrid1 do
  begin
    Cells[1,0]:= '������';
    Cells[2,0]:= '�������.';
    Cells[3,0]:= '�������';
    Cells[4,0]:= '������.��.';
    Cells[5,0]:= '������� ����';
  end;
end;

procedure TLab3.StringGrid1SetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
//������� ��������� ��� ���������� ��������������
// ������ (ACol, ARow). � ��������� Value ����������
// �������� ��������� ������ ��� �������������� ������.
var
b: real; j:byte;
//b - ������������ �������� ������ ��������������.
//j - �������� ����� ��� �������� ������ ������� ������� a.
begin 
 With StringGrid1 do
  Begin
   If   Value = ' ' then Exit; //���� ������ �� �������, �� ����� �� ���������
   //������ ����������� �����.
    Try b:=StrToFloat(Value)
    Except ShowMessage('�� ������ ������! ');
    b:=0;
    end; //����� ����������� �����.
    //������������ ������� ��������� ������.
    If (b<1) or (b>5) then begin
    Cells[ACol,ARow]:= ' ';
    Exit;
    end;
    Cells[ACol,ARow]:= Value;//�������� ������ � �������.
    a[ARow,ACol]:=b; //�������� ������ � ������.
    //����� ������� �������� �����.
    ss[ARow]:=0;
    for j:=1 to 4 do ss[ARow]:=ss[ARow] + a[ARow,j];
    ss[ARow]:= ss[ARow]/4;
    //���������� ������� ���� � 5-�� ������� �������.
    Cells[5,ARow]:= FormatFloat('##.##', ss[ARow])
    End;
end;

procedure TLab3.RadioGroup2Click(Sender: TObject);
 // ������� ��������� ��� ����� �������� �������
     var S:real; k, i: byte;
      begin
      S:=0; k:=0;
      With RadioGroup2 do
       Begin
       Case ItemIndex of
        0: begin          // ������ �������� ����� ������
            for i:= 1 to n do
            if p[i] = 'm' then begin S:=S + ss[i] ; k:=k + 1 end;
            if k=0 then S:=0 else S:=S/k;  end;
        1: begin          // ������ �������� ����� ������� 
            for i:= 1 to n do
            if p[i] = 'g' then begin S:=S + ss[i] ; k:=k + 1 end;
            if k=0 then S:=0 else S:=S/k;  end;
        2: begin          // ������ ������ �������� ����� 
           for i:= 1 to n do
            S:=S + ss[i] ;
            if n=0 then S:=0 else S:=S/n;      end; end;  end;
            // ����� ���������� � ���� ���������
            Edit2.Text := FormatFloat('##.##',S);
end;

procedure TLab3.N1Click(Sender: TObject);
type TClas = record f:string[30]; p:string[1]; ss: real; end;
var ff:File of TClas; Cl: TClas; i:integer;
begin
  if SaveDialog1.Execute then AssignFile (ff,SaveDialog1.FileName)
  else Exit;
  rewrite(ff);
  for i:= 1 to n do
  begin
    Cl.f:=f[i]; Cl.p:=p[i]; Cl.ss:= ss[i];
    Write(ff,Cl);
  end;
  CloseFile(ff);
end;

procedure TLab3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
PasswordDlg.Close;
end;

procedure TLab3.aaaaaa1Click(Sender: TObject);
var b:byte; a:string[8];
begin
  a:= (Sender As TComponent).Name;
  b:= StrToInt(Copy(a,7,1));
  case b of
    1: begin
    if lab2 = Nil then lab2:= Tlab2.Create(Self);
    lab2.Show;
    Lab3.Hide;
    end;
    2: begin
    if lab4 = Nil then lab4:= Tlab4.Create(Self);
    lab4.Show;
    Lab3.Hide;
    end;
    3: begin
    if MainForm = Nil then MainForm:= TMainForm.Create(Self);
    MainForm.Show;
    Lab3.Hide;
    end;
    4: close;
    end;
end;

end.
