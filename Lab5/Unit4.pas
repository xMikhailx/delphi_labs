unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, ExtDlgs;

type
  TLab4 = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    MainMenu1: TMainMenu;
    aaaaaa0: TMenuItem;
    aaaaaa1: TMenuItem;
    N31: TMenuItem;
    aaaaaa4: TMenuItem;
    aaaaaa2: TMenuItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    ColorDialog1: TColorDialog;
    FontDialog1: TFontDialog;
    N4: TMenuItem;
    aaaaaa3: TMenuItem;
    procedure aaaaaa1Click (Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Lab4: TLab4;

implementation

uses Unit2, Unit3, Unit6, Unit5;

{$R *.dfm}

procedure Tlab4.aaaaaa1Click (Sender: TObject);
var b:byte; a:string[8];
begin
  a:= (Sender As TComponent).Name;
  b:= StrToInt(Copy(a,7,1));
  case b of
    1: begin
    if lab2 = Nil then lab2:= Tlab2.Create(Self);
    lab2.Show;
    Lab4.Hide;
    end;
    2: begin
    if lab3 = Nil then lab3:= Tlab3.Create(Self);
    lab3.Show;
    Lab4.Hide;
    end;
    3: begin
    if MainForm = Nil then MainForm:= TMainForm.Create(Self);
    MainForm.Show;
    Lab4.Hide;
    end;
    4: close;
    end;
end;


procedure TLab4.N2Click(Sender: TObject);
begin
if FontDialog1.Execute then Panel1.Font := FontDialog1.Font;
end;

procedure TLab4.N1Click(Sender: TObject);
begin
if OpenPictureDialog1.Execute  and FileExists(OpenPictureDialog1.FileName)  then  Image1.Picture.LoadFromFile(OpenPictureDialog1.FileName); 
end;

procedure TLab4.N3Click(Sender: TObject);
begin
if ColorDialog1.Execute then Panel1.Color := ColorDialog1.Color;
end;

procedure TLab4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
PasswordDlg.Close;
end;

end.
