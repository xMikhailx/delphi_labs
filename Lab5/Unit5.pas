unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    aaaaaa1: TMenuItem;
    Lab31: TMenuItem;
    aaaaaa2: TMenuItem;
    N2: TMenuItem;
    aaaaaa3: TMenuItem;
    aaaaaa4: TMenuItem;
    Button1: TButton;
    procedure aaaaaa1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Unit2, Unit3, Unit4, Unit1, Unit6;

{$R *.dfm}

procedure TMainForm.aaaaaa1Click(Sender: TObject);
var b:byte; a:string[8];
begin
  a:= (Sender As TComponent).Name;
  b:= StrToInt(Copy(a,7,1));
  case b of
    1: begin
    if lab2 = Nil then lab2:= Tlab2.Create(Self);
    lab2.Show;
    MainForm.Hide;
    end;
    2: begin
    if lab3 = Nil then lab3:= Tlab3.Create(Self);
    lab3.Show;
    MainForm.Hide;
    end;
    3: begin
    if lab4 = Nil then lab4:= Tlab4.Create(Self);
    lab4.Show;
    MainForm.Hide;
    end;
    4: close;
    end;
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
AboutBox.Show;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
PasswordDlg.Hide;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
PasswordDlg.Close;
end;

end.
