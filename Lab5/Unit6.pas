unit Unit6;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons;

type
  TPasswordDlg = class(TForm)
    Label1: TLabel;
    Password: TEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PasswordDlg: TPasswordDlg;

implementation

uses Unit5;

{$R *.dfm}

procedure TPasswordDlg.CancelBtnClick(Sender: TObject);
begin
Close;
end;

procedure TPasswordDlg.OKBtnClick(Sender: TObject);
begin
if Password.Text = '1234' then MainForm.Show;
end;

end.

