object Form1: TForm1
  Left = 313
  Top = 159
  Width = 1200
  Height = 675
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDragDrop = FormDragDrop
  OnDragOver = FormDragOver
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 40
    Width = 505
    Height = 289
    Hint = #221#242#243' '#239#224#237#229#235#252' '#236#238#230#237#238' '#239#229#240#229#236#229#249#224#242#252
    Caption = 'Panel1'
    DragMode = dmAutomatic
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnMouseDown = FormMouseDown
    OnMouseMove = FormMouseMove
    OnMouseUp = FormMouseUp
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 161
      Height = 33
      Caption = 'Label1'
    end
    object Edit1: TEdit
      Left = 248
      Top = 32
      Width = 217
      Height = 21
      DragMode = dmAutomatic
      TabOrder = 0
      Text = 'Edit1'
      OnKeyPress = Edit1KeyPress
    end
  end
  object Panel2: TPanel
    Left = 544
    Top = 40
    Width = 593
    Height = 289
    Caption = 'Panel2'
    TabOrder = 1
    OnMouseDown = FormMouseDown
    OnMouseMove = FormMouseMove
    OnMouseUp = FormMouseUp
    object Label2: TLabel
      Left = 24
      Top = 16
      Width = 193
      Height = 41
      Caption = 'Label2'
    end
    object Edit2: TEdit
      Left = 328
      Top = 16
      Width = 233
      Height = 21
      TabOrder = 0
      Text = 'Edit2'
      OnDragDrop = Edit2DragDrop
      OnDragOver = Edit2DragOver
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 571
    Width = 1184
    Height = 65
    Panels = <
      item
        Width = 400
      end
      item
        Width = 400
      end
      item
        Width = 400
      end>
  end
end
