unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    StatusBar1: TStatusBar;
    procedure Edit2DragOver(Sender, Source: TObject; X,Y: integer; State: TDragState; var Accept: boolean);
    procedure FormDragOver(Sender, Source: TObject; X,Y: integer; State: TDragState; var Accept: boolean);
    procedure Edit2DragDrop(Sender, Source: TObject; X,Y: integer);
    procedure FormDragDrop(Sender, Source: TObject; X,Y: integer);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: integer);
    procedure FormMouseMove(Sender: TObject;  Shift: TShiftState; X,Y: integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Edit2DragOver(Sender, Source: TObject; X,Y: integer; State: TDragState; var Accept: boolean);
begin
// ���� �������� �������� ������������� ��������, �� Edit2
// ���������� ����������.
If Source.ClassName = 'TEdit' then Accept := True
Else Accept := False;
end;

procedure TForm1.FormDragOver(Sender, Source: TObject; X,Y: integer; State: TDragState; var Accept: boolean);
begin
// ���� �������� �������� ������, �� ����� ���������� ����������.
If Source.ClassName = 'TPanel' then Accept := True
Else Accept := False;
end;

procedure TForm1.Edit2DragDrop(Sender, Source: TObject; X,Y: integer);
begin
// �������� ���������� ��������� � ��������.
Edit2.Text := (Source As TEdit).Text;
end;

procedure TForm1.FormDragDrop(Sender, Source: TObject; X,Y: integer);
begin
// ������������� ��������� ���������� ����,
// ������� �� ����� � ������ ���������� ������.
(Source As TPanel).Left := x;
(Source As TPanel).Top := y;
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  If Key in ['a'.. 'z'] then begin
    Key := Chr(Ord(Key)-32); //������� �������������� � ������� �������
  end;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: integer);
begin
  // � ������ ������ ������ ������� ���������� ���������� ����.
  StatusBar1.Panels[0].Text := '������: '+ 'X: ' + IntToStr(x) + ';Y: ' +IntToStr(y);
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: integer);
begin
  // �� ������ ������ ������ ������� ���������� ���������� ����.
  StatusBar1.Panels[1].Text := '�����: '+ 'X: ' + IntToStr(x) + ';Y: ' +IntToStr(y);
End;

procedure TForm1.FormMouseMove(Sender: TObject;  Shift: TShiftState; X,Y: integer);
begin
  // � ������ ������ ������ ������� ���������� ���������� ����.
  StatusBar1.Panels[2].Text := 'X: ' + IntToStr(x) + ';Y: ' +IntToStr(y);
end;


end.
