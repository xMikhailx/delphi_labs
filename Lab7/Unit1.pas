unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ImgList, ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    ControlBar1: TControlBar;
    ImageList1: TImageList;
    ImageList2: TImageList;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolBar2: TToolBar;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PaintBox1: TPaintBox;
    ProgressBar1: TProgressBar;
    UpDown1: TUpDown;
    Animate1: TAnimate;
    Shape1: TShape;
    Timer1: TTimer;
    Edit1: TEdit;
    Label1: TLabel;
    Panel2: TPanel;
    Edit2: TEdit;
    MonthCalendar1: TMonthCalendar;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure ToolButton3Click(Sender: TObject);
    procedure MonthCalendar1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.BitBtn1Click(Sender: TObject);
var x,y: integer;
begin
  with PaintBox1.Canvas do
  begin
    Brush.Color:=clRed;
    Ellipse(0,0,(Width div 2),(Height div 2));
    Font.Size:=Height div 10;
    Font.Color:= clWhite;
    X:=(Width-TextWidth('Delphi')) div 4;
    Y:=(Height-TextHeight('D')) div 4;
    TextOut(x,y, 'Delphi');
end;
end;


procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  PaintBox1.Refresh;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
ProgressBar1.Position:= ProgressBar1.Position + 1;
If  ProgressBar1.Position >=  ProgressBar1.Max then
begin
Timer1.Enabled:=False;
ProgressBar1.Hide;
Animate1.Stop;
end;

end;

procedure TForm1.UpDown1Click(Sender: TObject; Button: TUDBtnType);
begin
//�������� �������� Position ���������� UpDown1 ���������� � Edit1.
Edit1.Text:=IntToStr(UpDown1.Position);
      Case UpDown1.Position mod 5 of
        //� ����������� �� �������� �������� Position ���������� UpDown1
       // ���������� ��� ������ � ���������� Shape1.
       0: Shape1.Shape:= stRectangle;
       1: Shape1.Shape:= stSquare;
       2: Shape1.Shape:= stRoundRect;
       3: Shape1.Shape:= stRoundSquare;
       4: Shape1.Shape:= stEllipse;
       5: Shape1.Shape:= stCircle;
end;

end;

procedure TForm1.ToolButton3Click(Sender: TObject);
begin
PageControl1.ActivePageIndex:=2;
        ProgressBar1.Position:=0;
        Animate1.Active:=True;
        ProgressBar1.Show;
        Timer1.Enabled:=True;

end;

procedure TForm1.MonthCalendar1Click(Sender: TObject);
begin
Edit2.Text:= DateToStr(MonthCalendar1.Date);
end;

end.
