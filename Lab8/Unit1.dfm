object Form1: TForm1
  Left = 289
  Top = 188
  Width = 535
  Height = 412
  Caption = #210#224#225#243#235#232#240#238#226#224#237#232#229' '#244#243#237#234#246#232#232
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF009999
    99999999999999999999999999999CC00000000000000000000000000CC99CC0
    00000000B00000000B0000000CC99CC00000000040000800040008080CC99CC0
    0000000030000000030000000CC99CC008080000C00000000C0008080CC99CC0
    00000000C00008080C0000000CC99CC008080800C00000000C0000000CC99CC0
    00000000C0080C00CC0000000CC99CCCC00C0000C0000C00CC0000000CC99CCC
    C00C0000C0000C00CC00000CCCC99CCCC00C0000C0000CCCCC00000CCCC99CCC
    CCCC0000C0030CCCCC00000CCCC99CCCCCCC0300C0000CCCCC00000CCCC99CCC
    CC9C0000CC00CCCCCC00000CCCC99CCCCCCC0300CCCCCCCCCC00000CCCC99CCC
    CCCC0000CCCFCCCCCC00080CCCC99CCCFCCC0808CCCCCCCCCCCC000CCCC99CCC
    CCCC0000CCCCCCCCFCCC080CCCC99CCCCCCC0808CCCCCCCCCCCC000CCFC99CCC
    CCCC0000CCCCCCCCCCCCC0CCCCC99CCCCCCC0808CCCCCCCCCCCCC0CCCCC99CCC
    CCCC0000CCCCFCCCCCCCC0CCCCC99CCCCCCCC00CCCCCCCCCCCCCCCCCCCC99CCC
    FCCCC00CCCCCCCCCFCCCCCCCCCC99CCCCCCCCCCCCCCCCCCCCCCCCC9CCCC99CCC
    CCCCCCCCCCCCCFCCCCCCCCCCCCC99CCCCCCCCCCCCCCCCCCCCFCCCCCCCCC99CCC
    CCCC6FCCCCCCCCCCCCCCCCCCFCC99CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC99CCC
    CCCCCCCCCCCCCCCCCCCCCCCCCCC9999999999999999999999999999999990000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  Menu = MainMenu1
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 360
    Top = 24
    Width = 76
    Height = 13
    Caption = #203#229#226#224#255' '#227#240#224#237#232#246#224
  end
  object Label2: TLabel
    Left = 360
    Top = 72
    Width = 82
    Height = 13
    Caption = #207#240#224#226#224#255' '#227#240#224#237#232#246#224
  end
  object Label3: TLabel
    Left = 360
    Top = 120
    Width = 20
    Height = 13
    Caption = #216#224#227
  end
  object Label4: TLabel
    Left = 206
    Top = 8
    Width = 107
    Height = 24
    Caption = 'y = sin(x) + 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 360
    Top = 48
    Width = 137
    Height = 21
    TabOrder = 0
    Text = '1'
  end
  object Edit2: TEdit
    Left = 360
    Top = 96
    Width = 137
    Height = 21
    TabOrder = 1
    Text = '2'
  end
  object Edit3: TEdit
    Left = 360
    Top = 144
    Width = 137
    Height = 21
    TabOrder = 2
    Text = '0.2'
  end
  object GroupBox1: TGroupBox
    Left = 360
    Top = 176
    Width = 137
    Height = 113
    Caption = #194#251#226#238#228
    TabOrder = 3
    object CheckBox1: TCheckBox
      Left = 8
      Top = 24
      Width = 121
      Height = 17
      Caption = #205#224' '#253#234#240#224#237#229
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 8
      Top = 48
      Width = 121
      Height = 17
      Caption = #194' '#244#224#233#235
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 8
      Top = 72
      Width = 121
      Height = 17
      Caption = #194' '#236#224#241#241#232#226
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
  end
  object Memo1: TMemo
    Left = 16
    Top = 40
    Width = 337
    Height = 257
    ScrollBars = ssBoth
    TabOrder = 4
  end
  object MainMenu1: TMainMenu
    Left = 448
    object N1: TMenuItem
      Caption = #194#251#247#232#241#235#232#242#252
      object N2: TMenuItem
        Caption = #210#224#225#243#235#232#240#238#226#224#242#252
        OnClick = N2Click
      end
      object N3: TMenuItem
        Caption = #206#247#232#241#242#232#242#252' '#239#238#235#229' '#226#251#226#238#228#224
        OnClick = N3Click
      end
    end
    object N4: TMenuItem
      Caption = #194#251#245#238#228
      object N5: TMenuItem
        Caption = #206' '#239#240#238#227#240#224#236#236#229
      end
      object N6: TMenuItem
        Caption = #194#251#245#238#228
        OnClick = N6Click
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    object N7: TMenuItem
      Caption = #206#247#232#241#242#232#242#252' '#239#238#235#229' '#226#251#226#238#228#224
      OnClick = N7Click
    end
  end
end
