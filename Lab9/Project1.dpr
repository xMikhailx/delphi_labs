program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {PagesDlg},
  Unit2 in 'Unit2.pas' {AboutBox};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TPagesDlg, PagesDlg);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.
