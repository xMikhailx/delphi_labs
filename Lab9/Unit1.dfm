object PagesDlg: TPagesDlg
  Left = 186
  Top = 132
  Width = 443
  Height = 367
  Caption = #210#229#235#229#244#238#237#237#251#233' '#241#239#240#224#226#238#247#237#232#234
  Color = clBtnFace
  ParentFont = True
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 427
    Height = 308
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    ParentColor = True
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 5
      Top = 5
      Width = 417
      Height = 298
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = #209#239#232#241#238#234' '#226#241#229#245' '#231#224#239#232#241#229#233
        object StringGrid1: TStringGrid
          Left = 0
          Top = 0
          Width = 409
          Height = 270
          Align = alClient
          ColCount = 4
          FixedCols = 0
          RowCount = 100
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          ColWidths = (
            97
            103
            96
            83)
        end
      end
      object TabSheet2: TTabSheet
        Caption = #208#229#228#224#234#242#232#240#238#226#224#237#232#229' '#232' '#239#238#232#241#234' '#231#224#239#232#241#229#233
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 155
          Height = 13
          Caption = #194#251#225#229#240#232#242#229' '#244#224#236#232#235#232#254' '#232#231' '#241#239#232#241#234#224
        end
        object Label2: TLabel
          Left = 8
          Top = 32
          Width = 93
          Height = 13
          Caption = #194#226#229#228#232#242#229' '#244#224#236#232#235#232#254
        end
        object Label3: TLabel
          Left = 216
          Top = 24
          Width = 24
          Height = 13
          Caption = #200#203#200
        end
        object Label4: TLabel
          Left = 24
          Top = 112
          Width = 32
          Height = 13
          Caption = 'Label4'
        end
        object ComboBox1: TComboBox
          Left = 256
          Top = 8
          Width = 137
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = NewEdit
        end
        object Edit1: TEdit
          Left = 256
          Top = 32
          Width = 137
          Height = 21
          TabOrder = 1
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 88
          Width = 385
          Height = 129
          Caption = #205#224#233#228#229#237#224' '#231#224#239#232#241#252
          TabOrder = 2
          object Label5: TLabel
            Left = 16
            Top = 16
            Width = 49
            Height = 13
            Caption = #212#224#236#232#235#232#255
          end
          object Label6: TLabel
            Left = 16
            Top = 72
            Width = 31
            Height = 13
            Caption = #192#228#240#229#241
          end
          object Label7: TLabel
            Left = 224
            Top = 16
            Width = 22
            Height = 13
            Caption = #200#236#255
          end
          object Label8: TLabel
            Left = 224
            Top = 72
            Width = 45
            Height = 13
            Caption = #210#229#235#229#244#238#237
          end
          object Edit2: TEdit
            Left = 16
            Top = 40
            Width = 113
            Height = 21
            TabOrder = 0
          end
          object Edit3: TEdit
            Left = 16
            Top = 96
            Width = 113
            Height = 21
            TabOrder = 1
          end
          object Edit4: TEdit
            Left = 224
            Top = 40
            Width = 113
            Height = 21
            TabOrder = 2
          end
          object Edit5: TEdit
            Left = 224
            Top = 96
            Width = 113
            Height = 21
            TabOrder = 3
          end
        end
        object Button1: TButton
          Left = 8
          Top = 232
          Width = 89
          Height = 25
          Caption = #207#240#229#228#251#228#243#249#232#233
          TabOrder = 3
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 104
          Top = 232
          Width = 89
          Height = 25
          Caption = #205#224#233#242#232
          TabOrder = 4
        end
        object Button3: TButton
          Left = 200
          Top = 232
          Width = 89
          Height = 25
          Caption = #211#228#224#235#232#242#252
          TabOrder = 5
        end
        object Button4: TButton
          Left = 296
          Top = 232
          Width = 89
          Height = 25
          Caption = #209#235#229#228#243#254#249#232#233
          TabOrder = 6
          OnClick = Button4Click
        end
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 320
    object N1: TMenuItem
      Caption = #212#224#233#235
      object N2: TMenuItem
        Caption = #207#240#238#247#232#242#224#242#252' '#232#231' '#244#224#233#235#224
        OnClick = N2Click
      end
      object N3: TMenuItem
        Caption = #199#224#239#232#241#224#242#252' '#226' '#244#224#233#235
        OnClick = N3Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object N11: TMenuItem
        Caption = #194#251#245#238#228
        OnClick = N11Click
      end
    end
    object N5: TMenuItem
      Caption = #207#240#224#226#234#224
      object N6: TMenuItem
        Caption = #209#238#240#242#232#240#238#226#224#242#252
        object N8: TMenuItem
          Caption = #192'->'#223
        end
        object N9: TMenuItem
          Caption = #223'->'#192
        end
      end
      object N7: TMenuItem
        Caption = #209#238#245#240#224#237#232#242#252' '#232#231#236#229#237#229#237#232#255
        OnClick = N7Click
      end
    end
    object N10: TMenuItem
      Caption = #206' '#239#240#238#227#240#224#236#236#229
      OnClick = N10Click
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 352
  end
  object SaveDialog1: TSaveDialog
    Left = 384
  end
end
