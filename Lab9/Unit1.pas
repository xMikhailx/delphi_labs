unit Unit1;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ComCtrls, ExtCtrls, Grids, Menus, Dialogs;

type
  TPagesDlg = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    StringGrid1: TStringGrid;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Edit2: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Edit3: TEdit;
    Label7: TLabel;
    Edit4: TEdit;
    Label8: TLabel;
    Edit5: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NewEdit(Sender: TObject);
    procedure NewComboBox;
    procedure N7Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
    Info = record {��� ������}
    familia, imja : string[20];
    adres : string[100];
    tel : string[15];
end;

var
  PagesDlg: TPagesDlg;
  F : file of info;        {���� �������}
  r : info; 		{���� ������}
  N : integer;      	{���������� �������}

implementation

uses Unit2;

{$R *.dfm}

procedure TPagesDlg.N10Click(Sender: TObject);
begin
  AboutBox.Show;
end;

procedure TPagesDlg.N11Click(Sender: TObject);
begin
  Close;
end;

procedure TPagesDlg.N2Click(Sender: TObject);
var i: integer;
begin
  {���������� ������������ ������� "Open file"}
  if OpenDialog1.Execute then begin
    {F - �������� ���������� �������� ����� (�������� FileName ������� OpenDialog1)}
    AssignFile(F, OpenDialog1.FileName);
    Reset(F);
    N := 0;       {������ � ������ �����} {��������� ��� ������ �� �����}
    while not eof(F) do
    begin {� ���������� r ��������� ������� ������}
      Read(F,r);
      {������� - � ������ ������� �������}
      StringGrid1.Cells[0, N+1] := r.familia; {��� - �� ������ ������� �������}
      StringGrid1.Cells[1,N+1] := r.imja;	{����� - � ������ ������� �������}
      StringGrid1.Cells[2,N+1] := r.adres; 	{������� - � ��������� ������� �������}
      StringGrid1.Cells[3,N+1] := r. tel;
      N := N + 1;      				{����������� ����� ����� ������� N}
    end;
    CloseFile(F);       {��������� ����}
    {��������� ���� ��������� ��������� ���� �������������� ������ �������� �����}
    NewEdit(ComboBox1);
    {��������� ���� ��������� ��������� ��������������� ������ ������ �������� �����}
    NewComboBox;
  end;
end;

procedure TPagesDlg.N3Click(Sender: TObject);
var i : integer;
begin
  if SaveDialog1.Execute then
  begin
    AssignFile(F, SaveDialog1.FileName);
    Rewrite(F);
    i:=1;
    while StringGrid1.Cells[3, i]<>'' do
    begin
      r.familia := StringGrid1.Cells[0, i];
      r.imja := StringGrid1.Cells[1, i];
      r.adres := StringGrid1.Cells[2, i];
      r.tel := StringGrid1.Cells[3, i];
      Write(F,r);
      i := i + 1;
    end;
    CloseFile(F);
  end;
end;

procedure TPagesDlg.FormCreate(Sender: TObject);
begin
  StringGrid1.cells[0, 0] := '�������';
  StringGrid1.cells[1, 0] := '���';
  StringGrid1.cells[2, 0] := '�����';
  StringGrid1.cells[3, 0] := '�������';
  {������� ������ ������ � ��������������� ������}
  ComboBox1.Items.Add(' ');
  {������������ ��� ������ �� ������ �������� ������ �����������}
  ComboBox1.ItemIndex:=0;
end;

procedure TPagesDlg.NewEdit(Sender: TObject);
var i : integer;
begin
  {i - ����� ������ ������ � �������}
  i := ComboBox1.ItemIndex + 1;
  if i>0 then
  begin
    {��� �������� ������� ���������������� ������}
    Edit2.Text := StringGrid1.Cells[0, i]; 	{�������� ������ 1-�� ������ �}
    Edit4.Text := StringGrid1.Cells[1, i]; 	{������ �������� ����� �}
    Edit3.Text := StringGrid1.Cells[2, i]; 	{��������������� ���� �� ������ ��������}
    Edit5.Text := StringGrid1.Cells[3, i];
  end;
end;

procedure TPagesDlg.NewComboBox;
var i, j : integer;
begin   	{���������� �������� ������� ���������������� ������ }
  j :=ComboBox1.ItemIndex;     {������� ��������������� ������}
  ComboBox1.Clear;
  for i:=1 to N do                {��� ���� N ������ I..}
    {� ����������� ��������������� ������ ���������  ��� ������� �� ������� ������� �������}
    ComboBox1.Items.Add(StringGrid1.Cells[0, i]);
  {��������� �� ������� j}
  ComboBox1.ItemIndex :=j;
end;


procedure TPagesDlg.N7Click(Sender: TObject);
var i : integer;
begin
  {���������� ����� �������� ������}
  i :=ComboBox1.ItemIndex+1;
  if PageControl1.ActivePage=TabSheet2 then
  begin
    {���� �������� ������ �������a}
    StringGrid1.Cells[0, i] := Edit2.Text;
    {�� ������ ��������� � ������ }
    StringGrid1.Cells[1, i] := Edit4.Text;
    StringGrid1. Cells[2, i] := Edit3.Text;
    StringGrid1.Cells[3, i] := Edit5.Text;
  end
  {����� - ������ ��������� �� ������ ��������}
  else
    NewEdit(ComboBox1);
  {� ����� ������� ��������� ��������������� ������}
  NewComboBox;
end;

procedure TPagesDlg.Button1Click(Sender: TObject);
begin
  if ComboBox1.Itemindex = 0 then                {����������� ������}
    ComboBox1.Itemindex := N-1
  else
    {� �������� ������ ���������� ����������� �� �������}
    ComboBox1.Itemindex := ComboBox1.Itemindex - 1; {��������� ������ "������� ������"}
  NewEdit(ComboBox1);
end;

procedure TPagesDlg.Button4Click(Sender: TObject);
begin
  if ComboBox1.Itemindex = ComboBox1.Items.Count-1 then                {����������� ������}
    ComboBox1.Itemindex := 0
  else
    {� �������� ������ ���������� ����������� �� �������}
    ComboBox1.Itemindex := ComboBox1.Itemindex + 1; {��������� ������ "������� ������"}
  NewEdit(ComboBox1);
end;

end.

