object Form1: TForm1
  Left = 338
  Top = 223
  Width = 1305
  Height = 675
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel
    Left = 880
    Top = 24
    Width = 83
    Height = 14
    Caption = 'Enter matrix size:'
  end
  object StringGrid1: TStringGrid
    Left = 64
    Top = 40
    Width = 737
    Height = 481
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 880
    Top = 48
    Width = 329
    Height = 22
    TabOrder = 1
    OnChange = Edit1Change
  end
  object Button1: TButton
    Left = 880
    Top = 104
    Width = 329
    Height = 41
    Caption = 'Calculate'
    TabOrder = 2
    OnClick = Button1Click
  end
end
