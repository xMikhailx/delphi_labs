unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Edit1: TEdit;
    Button1: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure FillIndexStringGrid(const Grid: TStringGrid);
var
  c, r: Integer;
begin
  for c := 1 to Pred(Grid.ColCount) do
  Grid.Cells[c, 0] := inttostr(c);
  for r := 1 to Pred(Grid.RowCount) do
  Grid.Cells[0, r] := inttostr(r);
end;

procedure TForm1.Button1Click(Sender: TObject);
var n,i,j,imax,jmax:integer;
    a:array of array of real;
    max:real;
begin
n:=strtoint(edit1.text);
StringGrid1.ColCount:=n+1;
StringGrid1.RowCount:=n+1;
Setlength(a,n,n);

for i:=0 to n-1 do
for j:=0 to n-1 do
a[i,j]:=strtofloat(StringGrid1.Cells[j+1,i+1]);

StringGrid1.ColCount:=n;
StringGrid1.RowCount:=n;

FillIndexStringGrid(StringGrid1);

max:=abs(a[0,0]); imax:=0; jmax:=0;
for i:=0 to n-1 do
for j:=0 to n-1 do
if (abs(a[i,j]))>max then
   begin
     max:=a[i,j];
     imax:=i;
     jmax:=j;
   end;
edit1.Text:='������������ �������='+floattostrf(max,fffixed,5,2)+
' � ������ '+inttostr(imax+1)+' � ������� '+ inttostr(jmax+1);
for i:=0 to n-1 do
for j:=jmax to n-2 do
   begin
     a[i,j]:=a[i,j+1];
   end;
for j:=0 to n-1 do
for i:=imax to n-2 do
   begin
     a[i,j]:=a[i+1,j];
   end;
for i:=0 to n-2 do
   begin
     for j:=0 to n-2 do
       begin
          StringGrid1.Cells[j+1,i+1]:=floattostrf(a[i,j],fffixed,5,2);
       end;
   end;
end;

procedure ClearStringGrid(const Grid: TStringGrid);
var
  c, r: Integer;
begin
  for c := 0 to Pred(Grid.ColCount) do
    for r := 0 to Pred(Grid.RowCount) do
      Grid.Cells[c, r] := '';
end;

function IsStrANumber(const S: string): Boolean; 
var
  P: PChar;
begin
  P      := PChar(S);
  Result := False;
  if (S = '') then Exit;
  while P^ <> #0 do
  begin
    if not (P^ in ['0'..'9']) then Exit;
    Inc(P);
  end;
  Result := True;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
if IsStrANumber(edit1.text) then
begin
  if (Length(edit1.text) > 2) then Exit;
  StringGrid1.ColCount:=strtoint(edit1.text)+1;
  StringGrid1.RowCount:=strtoint(edit1.text)+1;
  ClearStringGrid(StringGrid1);
  FillIndexStringGrid(StringGrid1);
end;
end;

end.
